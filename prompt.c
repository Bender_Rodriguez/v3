/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/26 17:44:22 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/30 18:03:25 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static int		check_sav(char **save, char **line)
{
	char	*temp;

	if ((temp = ft_strchr(*save, '\n')))
	{
		*temp = '\0';
		*line = ft_strdup(*save);
		ft_strcpy(*save, temp + 1);
		return (1);
	}
	return (0);
}

static int		check_rad(char *buf, char **save, char **line)
{
	char	*temp;

	if (!(*save))
		*save = "";
	if ((temp = ft_strchr(buf, '\n')))
	{
		*temp = '\0';
		*line = ft_strjoin(*save, buf);
		*save = ft_strdup(temp + 1);
		temp = NULL;
		return (1);
	}
	return (0);
}

static int			check_key(char buf[3], t_shell *shell)
{
	if (!ARROW && !SPECIAL)
	{
		ft_putchar(buf[0]);
		shell->cursor++;
		return (1);
	}
	else if (K_BAKSP)
	{
		special_key(shell, buf);
		return (-1);
	}
	else
		special_key(shell, buf);
	return (0);
}

static int			check_line(t_shell *shell, int const fd, char **line)
{
	static char		*save = NULL;
	char			*buf;
	int				r;
	int				ok;

	if (save)
		if (check_sav(&save, line))
			return (1);
	buf = ft_strnew(3);
	while ((r = read(fd, buf, 3)) > 0)
	{
		ok = check_key(buf, shell);
		buf[r] = '\0';
		if (check_rad(buf, &save, line))
			return (1);
		if (ok == 1)
			save = ft_strjoin(save, buf);
	}
	if (r == -1 || save == NULL)
		return (r == -1 ? -1 : 0);
	putcolor(shell->hist[shell->histindex - 1], CYAN, 1, 1);
	*line = ok == -1 ? ft_strdup(shell->hist[shell->histindex - 1])
		: ft_strdup(save);
	ft_strdel(&save);
	return (1);
}

void		prompt(t_shell *shell)
{
	prompter(shell);
	if (check_line(shell, 0, &(shell->cmd)) == 0)
		exit(0);
	if (check_cmd(shell->cmd))
		cmd_split(shell);
	else
	{
		//ft_putendl(shell->cmd);
		if (shell->cmd)
		{
			ft_shell(shell);
			shell->hist[shell->histindex] = ft_strdup(shell->cmd);
			shell->histindex++;
		}
	}
	shell->cursor = 0;
	free(shell->cmd);
	shell->cmd = NULL;
}
