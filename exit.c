/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/08 16:38:43 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/26 17:47:08 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void			free_env(char ***env)
{
	int			i;

	i = 0;
	while ((*env)[i])
	{
		free((*env)[i]);
		i++;
	}
	free(*env);
}

void			free_hash(t_sh **sh)
{
	t_sh		*temp;

	while (*sh)
	{
		temp = *sh;
		*sh = temp->next;
		free(temp->name);
		free(temp->path);
		free(temp);
	}
}

void			free_built(char **builtin)
{
	int			i;

	i = 0;
	while (builtin[i])
	{
		free(builtin[i]);
		i++;
	}
}

void			ft_exit(t_shell **shell, char ***arg)
{
	int			i;

	i = 0;
	free_env(&((*shell)->env));
	free((*shell)->path);
	free((*shell)->pwd);
	free((*shell)->old_pwd);
	free((*shell)->cmd);
	free((*shell)->home);
	free_hash(&((*shell)->sh));
	free_built((*shell)->builtin);
	free(*shell);
	i = 0;
	while (i < ft_tablen(*arg))
	{
		free((*arg)[i]);
		i++;
	}
	free(*arg);
	exit(0);
}
