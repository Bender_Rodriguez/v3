/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 23:57:31 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/26 17:46:47 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char		*get_path(char *arg, t_sh *sh)
{
	if (access(arg, F_OK) == 0)
	{
		while (sh)
		{
			if (strcmp(sh->path, arg) == 0)
				sh->time++;
			sh = sh->next;
		}
		return (arg);
	}
	else
	{
		while (sh)
		{
			if (ft_strcmp(sh->name, arg) == 0)
			{
				sh->time++;
				return (sh->path);
			}
			sh = sh->next;
		}
	}
	return (NULL);
}

int			is_built(char *arg, t_shell *shell)
{
	int		i;

	i = 0;
	while (shell->builtin[i])
	{
		if (ft_strcmp(arg, shell->builtin[i]) == 0)
			return (1);
		i++;
	}
	return (0);
}

void		ft_fork(char *path, char **arg, t_shell *shell)
{
	pid_t	pid;
	int		i;

	pid = fork();
	if (pid > 0)
	{
		waitpid(pid, &i, 0);
		get_stat(i);
	}
	if (pid == 0)
	{
		execve(path, arg, shell->env);
		exit (1);
	}
}

void		ft_shell(t_shell *shell)
{
	char	**arg;
	char	*path;
	int		i;

	arg = ft_space_split(shell->cmd);
	if (arg && arg[0])
	{
		i = 0;
		if (is_built(arg[0], shell))
			built(arg, shell);
		else if ((path = get_path(arg[0], shell->sh)))
		{
			ls_check(&path);
			ft_fork(path, arg, shell);
		}
		else
		{
			ft_putstr_fd("ft_minishell: command not found: ", 2);
			ft_putendl_fd(arg[0], 2);
		}
		while (i < ft_tablen(arg))
			free(arg[i++]);
		free(arg);
	}
}

void		cmd_split(t_shell *shell)
{
	char	**array;
	int		i;

	i = 0;
	array = ft_strsplit(shell->cmd, ';');
	while (array[i])
	{
		free(shell->cmd);
		shell->cmd = NULL;
		shell->cmd = ft_strdup(array[i]);
		ft_shell(shell);
		i++;
	}
}
