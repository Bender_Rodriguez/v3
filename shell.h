/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/26 17:45:07 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/30 15:07:56 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H
# define SHELL_H

# include "libft.h"
# include "colors.h"
# include <dirent.h>
# include <sys/wait.h>
# include <term.h>
# include <termcap.h>
# include <termios.h>
# include <signal.h>
# include <curses.h>
# include <sys/ioctl.h>

# define IN			0
# define OUT		1
# define LEFT		0
# define RIGHT		1
# define X			0
# define Y			1
# define K_UP		(buf[0] == 27 && buf[1] == 91 && buf[2] == 65)
# define K_DOWN		(buf[0] == 27 && buf[1] == 91 && buf[2] == 66)
# define K_RIGHT	(buf[0] == 27 && buf[1] == 91 && buf[2] == 67)
# define K_LEFT		(buf[0] == 27 && buf[1] == 91 && buf[2] == 68)
# define K_END		(buf[0] == 27 && buf[1] == 91 && buf[2] == 70)
# define K_HOME		(buf[0] == 27 && buf[1] == 91 && buf[2] == 72)
# define K_ENTER	(buf[0] == 10 && !buf[1] && !buf[2])
# define K_ECHAP	(buf[0] == 27 && !buf[1] && !buf[2])
# define K_SPACE	(buf[0] == 32 && !buf[1] && !buf[2])
# define K_BAKSP	(buf[0] == 127 && !buf[1] && !buf[2])
# define K_DEL		(buf[0] == 27 && buf[1] == 91 && buf[2] == 51)
# define ARROW		(K_LEFT || K_RIGHT || K_DOWN || K_UP)
# define SPECIAL	(K_BAKSP || K_ECHAP || K_DEL || K_HOME || K_END)
# define CURR		(shell->hist_curr)
# define HISTORIC	(CURR >= 0 && CURR < ft_tablen(shell->hist))
# define BAKSP		cursor[2]

typedef struct			s_sh
{
	char				*name;
	char				*path;
	int					time;
	struct s_sh			*next;
}						t_sh;

typedef struct			s_trial
{
	struct termios		term;
	int					size_ok;
	int					cols;
	int					lins;
	int					width;
	int					heigh;
}						t_trial;

typedef struct			s_shell
{
	int					limit;
	int					cursor;
	int					prompt;
	t_trial				trial;
	char				**env;
	char				*home;
	char				*path;
	char				*pwd;
	char				*old_pwd;
	t_sh				*sh;
	char				*cmd;
	char				*builtin[8];
	char				**hist;
	int					histindex;
}						t_shell;

void					special_key(t_shell *shell, char buf[3]);
void					prompter(t_shell *shell);
t_shell					*init_all(char **environ);
void					prompt(t_shell *shell);
int						tputchar(int c);
int						terminit(t_shell *shell);
int						termclose(t_shell *shell);
void					ls_check(char **path);
void					get_stat(int i);
int						check_cmd(char *cmd);
void					cmd_split(t_shell *shell);
void					init_shell(t_shell **shell, char **environ);
void					ft_sh(t_shell **shell);
void					push(t_sh **sh, char *name, char *path);
t_sh					*new_h(char *name, char *path);
void					ft_shell(t_shell *shell);
char					*get_path(char *arg, t_sh *sh);
int						is_built(char *arg, t_shell *shell);
void					ft_fork(char *path, char **arg, t_shell *shell);
void					built(char **arg, t_shell *shell);
void					ft_cd(t_shell **shell, char **arg);
void					ft_env(t_shell *shell);
void					ft_direct(t_shell **shell, char *path, char *arg);
void					save(t_shell **shell);
void					ft_setenv(char *name, char *content, t_shell **shell);
void					new_env(t_shell **shell, char *str);
void					free_env(char ***env);
void					free_hash(t_sh **sh);
void					free_built(char **builtin);
void					ft_exit(t_shell **shell, char ***arg);
void					ft_checkenv(char **arg, t_shell **shell, int i);
void					ft_unsetenv(char **arg, t_shell **shell);
void					delete_env(t_shell **shell, char **arg, int i);

#endif
