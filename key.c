/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 11:12:06 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/30 16:44:33 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static void		the_specialist(t_shell *shell, char buf[3])
{
	if (K_BAKSP && shell->cursor)
	{
		tputs(tgetstr("le", NULL), 1, tputchar);
		tputs(tgetstr("dc", NULL), 1, tputchar);
		shell->cursor--;
	}
}

static void		historic(t_shell *shell, char buf[3])
{
	int				i;

	i = shell->trial.cols - shell->cursor - 9;
	if (K_UP && shell->histindex > 0)
	{
		tputs(tgetstr("dl", NULL), 1, tputchar);
		while (i)
		{
			tputs(tgetstr("le", NULL), 1, tputchar);
			i--;
		}
		prompter(shell);
		ft_putstr(shell->hist[shell->histindex - 1]);
		shell->cmd = ft_strdup(shell->hist[shell->histindex - 1]);
		shell->histindex--;
	}
}

static void		the_arrow(t_shell *shell, char buf[3])
{
	if (K_LEFT && shell->cursor)
	{
		tputs(tgetstr("le", NULL), 1, tputchar);
		shell->cursor--;
	}
	else if (K_RIGHT)
	{
		tputs(tgetstr("nd", NULL), 1, tputchar);
		shell->cursor++;
	}
	else if (K_UP || K_DOWN)
		historic(shell, buf);
}

void			special_key(t_shell *shell, char buf[3])
{
	if (ARROW)
		the_arrow(shell, buf);
	if (SPECIAL)
		the_specialist(shell, buf);
}
